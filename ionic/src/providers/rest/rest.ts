import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {

  constructor(public http: HttpClient) {
    console.log('Hello RestProvider Provider');
  }

  login(dataValue) {
    let body = new URLSearchParams();
    body.set('username', dataValue.username);
    body.set('password', dataValue.password);
    return new Promise((resolve, reject) => {
      this.http.post(
        'https://voco.000webhostapp.com/index.php/user/login',
        body.toString(),
        {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        }
      ).subscribe(
        response => { resolve(response); },
        error => { reject(error); }
      );
    });
  }

  register(dataValue) {
    let body = new URLSearchParams();
    body.set('username', dataValue.username);
    body.set('password', dataValue.password);
    body.set('email',dataValue.email);
    body.set('first_name',dataValue.first_name);
    body.set('last_name',dataValue.last_name);
    body.set('birth_date',dataValue.birth_date);
		
    return new Promise((resolve, reject) => {
      this.http.post(
        'https://voco.000webhostapp.com/index.php/user/register',
        body.toString(),
        {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        }
      ).subscribe(
        response => { resolve(response); },
        error => { reject(error); }
      );
    });
  }
}
