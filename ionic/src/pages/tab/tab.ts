import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MediaCapture } from '@ionic-native/media-capture';
import { Media, MediaObject } from '@ionic-native/media';
import { Storage } from '@ionic/storage';
import { File } from '@ionic-native/file';
import { Chooser } from '@ionic-native/chooser';
import { ToastController } from 'ionic-angular';
import { MyprofilePage } from '../myprofile/myprofile';
import { HomePage } from '../home/home';
import { UploadsoundPage } from '../uploadsound/uploadsound';
import { LoginPage } from '../login/login';



@IonicPage()
@Component({
  selector: 'page-tabs',
  template: `
  <ion-tabs>
    <ion-tab [root]="homePage" tabTitle="Home" tabIcon="home"></ion-tab>
    <ion-tab [root]="uploadPage" tabIcon="mic"></ion-tab>
    <ion-tab [root]="myprofilePage" tabTitle="MyProfile" tabIcon="person"></ion-tab>
  </ion-tabs>
  `
})
export class TabsPage {

  myprofilePage = MyprofilePage;
  homePage = HomePage;
  uploadPage= UploadsoundPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage:Storage) {
  }

  ionViewDidLoad(){
    // this.storage.get('token').then(token => {
    //    if(token == null) {
    //      this.navCtrl.push(LoginPage);
    //    }
    //    else{
    //      this.navCtrl.setRoot(TabsPage);
    //    }
    //  });
   }

}
