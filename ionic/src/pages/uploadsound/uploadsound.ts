import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MediaCapture } from '@ionic-native/media-capture';
import { Media, MediaObject } from '@ionic-native/media';
import { Storage } from '@ionic/storage';
import { File } from '@ionic-native/file';
import { Chooser } from '@ionic-native/chooser';
import { ToastController } from 'ionic-angular';
import { PosttypePage } from '../posttype/posttype';

const MEDIA_FILES_KEY = 'mediaFiles';
const MEDIA_FILES_KEY2 = 'podcast';

@Component({
  selector: 'page-uploadsound',
  templateUrl: 'uploadsound.html',
})
export class UploadsoundPage {
  //mediaFiles =[];
  constructor(public navCtrl: NavController, private mediaCapture: MediaCapture,
    private storage:Storage, private media:Media, public toastCtrl: ToastController,
    private file:File, private chooser :Chooser) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadsoundPage');
  }

  uploadAudio(data){
    this.chooser.getFile('audio/mp3')
  .then(file => {
   // console.log(file ? file.name : 'canceled');
    this.presentToast('hai');
     
   // this.storeMediaFiles(file);
  })
  .catch((error: any) => 
    //console.error(error);
    this.presentToast('error')
  );
  }

  uploadAudio2(data){

  }

  presentToast(res) {
    const toast = this.toastCtrl.create({
    message: res,
    duration: 2000,
    position: 'bottom'
    });
    toast.present();
    }

    goToPosttype(){
      this.navCtrl.push(PosttypePage);
    }

}
