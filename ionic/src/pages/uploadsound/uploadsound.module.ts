import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadsoundPage } from './uploadsound';

@NgModule({
  declarations: [
    UploadsoundPage,
  ],
  imports: [
    IonicPageModule.forChild(UploadsoundPage),
  ],
})
export class UploadsoundPageModule {}
