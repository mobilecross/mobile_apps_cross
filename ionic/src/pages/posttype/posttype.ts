import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MediaCapture } from '@ionic-native/media-capture';
import { Media, MediaObject } from '@ionic-native/media';
import { Storage } from '@ionic/storage';
import { File } from '@ionic-native/file';
import { Chooser } from '@ionic-native/chooser';
import { ToastController } from 'ionic-angular';
import { HomePage } from '../home/home';
/**
 * Generated class for the PosttypePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

const MEDIA_FILES_KEY = 'mediaFiles';
const MEDIA_FILES_KEY2 = 'podcast';

@Component({
  selector: 'page-posttype',
  templateUrl: 'posttype.html',
})
export class PosttypePage {
  mediaFiles =[];
  constructor(public navCtrl: NavController, private mediaCapture: MediaCapture,
    private storage:Storage, private media:Media, public toastCtrl: ToastController,
    private file:File, private chooser :Chooser) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PosttypePage');
  }

  captureAudio1(){
    this.mediaCapture.captureAudio().then(res=>{
        this.storeMediaFiles(res);
        //this.presentToast(res);
    });
  }


  captureAudio2(data){
    this.mediaCapture.captureAudio().then(res=>{
      this.storeMediaFiles2(res);
      //this.presentToast(res);
  });
  }

  storeMediaFiles(files){
    this.storage.get(MEDIA_FILES_KEY).then(res=>{
      if(res){
        let arr = JSON.parse(res);
        arr = arr.concat(files);
        this.storage.set(MEDIA_FILES_KEY, JSON.stringify(arr));
      }
      else{
        this.storage.set(MEDIA_FILES_KEY, JSON.stringify(files));
      }
     // this.mediaFiles = this.mediaFiles.concat(files);
    });
    this.navCtrl.push(HomePage);
  }

  storeMediaFiles2(files){
    this.storage.get(MEDIA_FILES_KEY2).then(res=>{
      if(res){
        let arr = JSON.parse(res);
        arr = arr.concat(files);
        this.storage.set(MEDIA_FILES_KEY2, JSON.stringify(arr));
      }
      else{
        this.storage.set(MEDIA_FILES_KEY2, JSON.stringify(files));
      }
      //this.mediaFiles = this.mediaFiles.concat(files);
    });
    this.navCtrl.push(HomePage);
  }

  presentToast(res) {
    const toast = this.toastCtrl.create({
    message: res,
    duration: 2000,
    position: 'bottom'
    });
    toast.present();
    }
}
