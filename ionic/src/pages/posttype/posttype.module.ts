import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PosttypePage } from './posttype';

@NgModule({
  declarations: [
    PosttypePage,
  ],
  imports: [
    IonicPageModule.forChild(PosttypePage),
  ],
})
export class PosttypePageModule {}
