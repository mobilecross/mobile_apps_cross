import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MediaCapture } from '@ionic-native/media-capture';
import { Media, MediaObject } from '@ionic-native/media';
import { Storage } from '@ionic/storage';
import { File } from '@ionic-native/file';
import { Chooser } from '@ionic-native/chooser';
import { ToastController } from 'ionic-angular';
import { StreamingMedia, StreamingAudioOptions } from '@ionic-native/streaming-media';

const MEDIA_FILES_KEY = 'mediaFiles';
const MEDIA_FILES_KEY2 = 'podcast';
import { LoginPage } from '../login/login';
import { TabsPage } from '../tab/tab';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  mediaFiles =[];
  podcast =[];
  audioFile: MediaObject;
  constructor(public navCtrl: NavController, private mediaCapture: MediaCapture,
    private storage:Storage, private media:Media,public toastCtrl: ToastController,
    private file:File, private chooser :Chooser,private streamingMedia: StreamingMedia) {

  }

  ionViewWillEnter(){
  //  this.storage.get('token').then(token => {
  //     if(token == null) {
  //       this.navCtrl.push(LoginPage);
  //     }
  //     else{
  //       this.navCtrl.setRoot(TabsPage);
  //     }
  //   });

    this.storage.get(MEDIA_FILES_KEY).then(res=>{
      this.mediaFiles = JSON.parse(res) || [];
    });

    this.storage.get(MEDIA_FILES_KEY2).then(res=>{
      this.podcast = JSON.parse(res) || [];
    });

  }

  captureAudio(){
    this.mediaCapture.captureAudio().then(res=>{
        this.storeMediaFiles(res);
        this.presentToast(res);
    });
  }

  uploadAudio(){
    
  }

  cariAudio(){
    
    let file =this.chooser.getFile('audio/mp3');
    this.presentToast(file);
    /*this.chooser.getFile('audio/mp3')
  .then(file => {
   // console.log(file ? file.name : 'canceled');
    this.presentToast('hai');
     
   // this.storeMediaFiles(file);
  })
  .catch((error: any) => 
    //console.error(error);
    this.presentToast('error')
  );*/

  }
  
  play(myFile){
    if(this.audioFile){
      this.audioFile.stop();
    }
    console.log('play : ', myFile);
    if(myFile.name.indexOf('.mp3')){
      this.audioFile = this.media.create(myFile.localURL);
      this.audioFile.play();
    }
    // let options: StreamingAudioOptions = {
    //   successCallback: () => { console.log('Video played') },
    //   errorCallback: (e) => { console.log('Error streaming') },
    //   bgColor:"#c46fbb",
    //   keepAwake:true
    // };
    
    // this.streamingMedia.playAudio('http://soundbible.com/2219-Airplane-Landing-Airport.html', options);
  }

  storeMediaFiles(files){
    this.storage.get(MEDIA_FILES_KEY).then(res=>{
      if(res){
        let arr = JSON.parse(res);
        arr = arr.concat(files);
        this.storage.set(MEDIA_FILES_KEY, JSON.stringify(arr));
      }
      else{
        this.storage.set(MEDIA_FILES_KEY, JSON.stringify(files));
      }
      this.mediaFiles = this.mediaFiles.concat(files);
    });
  }

  presentToast(res) {
    const toast = this.toastCtrl.create({
    message: res,
    duration: 2000,
    position: 'bottom'
    });
    toast.present();
    }

}
