import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';

import { SignupPage } from '../signup/signup';
import { TabsPage } from '../tab/tab';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private rest: RestProvider, private storage: Storage, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  presentToast(res) {
    const toast = this.toastCtrl.create({
    message: res,
    duration: 2000,
    position: 'bottom'
    });
    toast.present();
    }

  login(data) {
    //this.presentToast('okok'); 
    this.rest.login(data.value)
      .then(response => {
        console.log(response);
        if(response['status'] == 'LOGIN_SUCCESS') {
          this.storage.set('token', response['token']);
          this.presentToast('Login Success');
          this.navCtrl.pop();
        }
        else{
          this.presentToast('Login Fail'); 
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  gotoSignUp(){
    this.navCtrl.push(SignupPage);
  }
}
