import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {MediaCapture} from '@ionic-native/media-capture';
import {Media} from '@ionic-native/media';
import {File} from '@ionic-native/file';
import {Chooser} from '@ionic-native/chooser';
import { StreamingMedia} from '@ionic-native/streaming-media';
import {IonicStorageModule} from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tab/tab';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { MyprofilePage } from '../pages/myprofile/myprofile';
import { UploadsoundPage } from '../pages/uploadsound/uploadsound';
import { RestProvider } from '../providers/rest/rest';
import { EditprofilePage } from '../pages/editprofile/editprofile';
import { PosttypePage } from '../pages/posttype/posttype';

import { UploadimagePage } from '../pages/uploadimage/uploadimage';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    LoginPage,
    SignupPage,
    MyprofilePage,
    UploadsoundPage,
    EditprofilePage,
    UploadimagePage,
    PosttypePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    LoginPage,
    SignupPage,
    MyprofilePage,
    UploadsoundPage,
    EditprofilePage,
    UploadimagePage,
    PosttypePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Media,
    MediaCapture,
    File,
    RestProvider,
    Chooser,
    StreamingMedia
  ]
})
export class AppModule {}
