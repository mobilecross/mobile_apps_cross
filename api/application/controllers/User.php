<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class User extends CI_Controller { 
	private function random_string($length) { 
		$str = ''; 
		while(--$length >= 0) { 
			$random_number = rand(48, 109); 
			if($random_number > 57) $random_number += 7; 
			if($random_number > 90) $random_number += 6; 
			$str .= chr($random_number); 
		} 
		return $str; 
	} 
 
	private function token_string($any) { 
		return $this->random_string(10) . $any . $this->random_string(12) . date('wHsyidm'); 
	} 
 
	private function generate_token($username) { 
		$user = $this->db->get_where('users', array('username' => $username), 1)->row(); 
		if($user != NULL) { 
			$token = $this->token_string($user->id); 
			$this->db->set(array('token' => $token))->where('id', $user->id); 
			$this->db->update('users'); 
			return $token; 
		} else return 'ERROR'; 
	} 
 
	private function send_activation_email($token) { 
		$email_message = '<p>Thank you for registering Voco!</p>'. 
						 '<p>'. 
							'<a href="http://voco.000webhost.com/index.php/user/activate/' . $token .'">Click here</a> to verify your account'. 
						 '</p>'; 
 
		$this->load->library('email'); 
		$this->email->from('voco@post.com'); 
		$this->email->to($this->input->post('email')); 
		$this->email->subject('Voco Account Verification'); 
		$this->email->message($email_message); 
		$this->email->send(); 
	} 

	public function login() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$user = $this->db->get_where(
			'users',
			array(preg_match('/.+@.+\..+/', $username) ? 'email' : 'username' => $username),
			1
		)->row();

		if($user != NULL) {
			if(!$user->activated) {
				echo json_encode(array(
					'status' => 'USER_IS_NOT_ACTIVATED'
				));
			} else if(password_verify($password, $user->password)) {
				$generated_token = $this->generate_token($user->username);
				$this->db->set(array('logged_in' => true))->where('id', $user->id);
				$this->db->update('users');
				echo json_encode(array(
					'status' => 'LOGIN_SUCCESS',
					'token' => $generated_token
				));
			} else {
				echo json_encode(array(
					'status' => 'LOGIN_FAIL'
				));
			}
		} else {
			echo json_encode(array(
				'status' => 'LOGIN_FAIL'
			));
		}
	}

	public function is_logged_in() {
		$token = $this->input->post('token');
		$user = $this->db->get_where('users', array('token' => crypt($token, $this->TOKEN_SALT)), 1)->row();
		if($user != NULL) {
			echo $user->logged_in ? 'LOGGED_IN' : 'LOGGED_OUT';
		} else echo 'THIS_DEVICE_IS_NOT_LOGGED_IN';
	}

	public function logout() {
		$username = $this->input->post('username');
		if($this->db->get_where('users', array('username' => $username), 1)->row() != NULL) {
			$this->db->set('logged_in', false)->where('username', $username);
			$this->db->update('users');
			echo 'SUCCESS';
		} else echo 'FAIL';
	}

	public function register(){
		$this->form_validation->set_rules(array(
			array(
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|is_unique[users.username]|alpha_dash',
				'errors' => array(
					'required' => 'USERNAME_REQUIRED',
					'is_unique' => 'USERNAME_EXISTS',
					'alpha_dash' => 'USERNAME_CONTAINS_UNALLOWED_CHARACTER'
				)
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|min_length[8]',
				'errors' => array(
					'required' => 'PASSWORD_REQUIRED',
					'min_length' => 'PASSWORD_TOO_SHORT'
				)
			),
			array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|valid_email', // manually validate unique email
				'errors' => array(
					'required' => 'EMAIL_REQUIRED',
					'valid_email' => 'EMAIL_INVALID'
				)
			),
			array(
				'field' => 'firstName',
				'label' => 'First name',
				'rules' => 'required',
				'errors' => array(
					'required' => 'FIRST_NAME_REQUIRED'
				)
			),
			array(
				'field' => 'birthDate',
				'label' => 'Birth date',
				'rules' => array(
					'required',
					'regex_match[/\d\d\d\d-(0[1-9]|1[0-2])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])/]'
				),
				'errors' => array(
					'required' => 'BIRTH_DATE_REQUIRED',
					'regex_match' => 'BIRTH_DATE_INVALID'
				)
			)
		));

		if($this->form_validation->run()) {
			$email = htmlentities($this->input->post('email'));
			$token = $this->token_string(htmlentities($this->input->post('username')));

			$user = $this->db->get_where('users', array('email' => $email), 1)->row(); // check if email exists
			if($user != NULL) {
				if($user->activated) echo 'EMAIL_EXISTS';
				else {
					$this->db->set(array(
						'username' => htmlentities($this->input->post('username')),
						'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
						'first_name' => htmlentities($this->input->post('firstName')),
						'last_name' => htmlentities($this->input->post('lastName')),
						'birth_date' => htmlentities($this->input->post('birthDate')),
						'logged_in' => false,
						'activated' => false,
						'token' => crypt($token, $this->TOKEN_SALT)
					))->where('id', $user->id);
					$this->db->update('users');
					// $this->send_activation_email($token);
					echo 'SUCCESS';
				}
			} else {
				$this->db->insert('users', array(
					'username' => htmlentities($this->input->post('username')),
					'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
					'email' => $email,
					'first_name' => htmlentities($this->input->post('firstName')),
					'last_name' => htmlentities($this->input->post('lastName')),
					'birth_date' => htmlentities($this->input->post('birthDate')),
					'logged_in' => false,
					'activated' => false,
					'token' => crypt($token, $this->TOKEN_SALT)
				));
				// $this->send_activation_email($token);
				echo 'SUCCESS';
			}
		} else echo validation_errors();
	}

	public function activate($token) {
		if($this->db->get_where('users', array('token' => crypt($token, $this->TOKEN_SALT)), 1)->row() != NULL) {
			$this->db->set('activated', true)->where('token', $token);
			$this->db->update('users');
			echo 'SUCCESS';
		} else echo 'FAIL';
	}

	function getMyProfile(){
		$result=$this->User_model->getMyProfile(1);
		echo $result['username'];
	}

	function getPersonProfile($username){
		$result=$this->User_model->getPersonProfile('udin');
		echo $result;
	}


}
