<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Token extends CI_Controller { 
    protected $user; 
    
    public function __construct() { 
        parent::__construct(); 
        $this->user = $this->db->get_where('users', array('token' => $this->input->post('token')), 1)->row(); 
        if($this->user == NULL || !$this->user->activated) { 
            echo 'UNAUTHORIZED'; 
            die(); 
        } 
    } 
} 