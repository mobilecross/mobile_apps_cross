<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
require(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'Token.php'); 

class Podcast extends Token{
    public function index() {
        echo 'Podcast API';
    }

    function getMyPodcast($username){
        $sql="select * from podcast where username='".$username."'";
		$qry = $this->db->query($sql);
		return $qry->result_array();
    }
    
    /**
     * Required post:
     * - token <string>
     * - podcastFile (mp3) <file>
     * - title <string>
     * - description <string>
     * - duration (seconds) <int>
     */
    public function upload_file() { 
        $get_max_podcast_id = $this->db->select_max('id_podcast')->get('podcasts')->row(); 
        $new_podcast_id = 0; 
        if(isset($get_max_podcast_id->id_podcast)) $new_podcast_id = $get_max_podcast_id->id_podcast + 1; 
 
        $upload_path = './upload/podcasts/' . $this->user->username; 
        if(!is_dir($upload_path)) mkdir($upload_path); 
 
        $this->load->library('upload', array( 
            'upload_path' => $upload_path, 
            'allowed_types' => 'mp3', 
            'file_name' => htmlentities($this->input->post('title')) . '--' . $new_podcast_id 
        )); 
 
        if(is_numeric($this->input->post('duration')) && $this->upload->do_upload('podcastFile')) { 
            $file_path = $this->upload->data()['full_path']; // use full_path

            $this->db->insert('podcasts', array(
                'title' => htmlentities($this->input->post('title')),
                'description' => htmlentities($this->input->post('description')),
                'username' => $this->user->username,
                'uploaded_date' => date('Y-m-d'),
                'duration' => $this->input->post('duration'),
                'file_path' => $file_path
            ));

            echo json_encode(array(
                'status' => 'SUCCESS',
                'filePath' => $file_path
            ));
        } else {
            echo json_encode(array(
                'status' => 'FAIL',
                'errors' => $this->upload->display_errors('  --', '--  ')
            ));
        }
    }
}